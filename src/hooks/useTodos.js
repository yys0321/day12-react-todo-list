import { useDispatch } from "react-redux";
import { getTodos, postTodo, updateTodo, removeTodo, editTodo } from "../api/todos";
import { loadItem } from "../app/todoSlice";

export const useTodos = () => {
    const dispatch = useDispatch();

    const loadTodos = () => {
        getTodos().then((response) => {
            dispatch(loadItem(response.data));
        });
    };

    const createTodo = async (todo) => {
        await postTodo(todo);
        loadTodos();
    };

    const toggleTodo = async (todo) => {
        await updateTodo(todo);
        loadTodos();
    };

    const deleteTodo = async (todo) => {
        await removeTodo(todo.id);
        loadTodos();
    };

    const changeTodo = async (todo) => {
        await editTodo(todo);
        loadTodos();
    };

    return {
        loadTodos,
        createTodo,
        toggleTodo,
        deleteTodo,
        changeTodo
    };
};
import client from "./client";

export const getTodos = () => {
    return client.get("/todos/");
};

export const postTodo = (newTodo) => {
    return client.post("/todos", newTodo);
};

export const updateTodo = (todo) => {
    return client.put(`/todos/${todo.id}`, todo);
};

export const removeTodo = (todoId) => {
    return client.delete(`/todos/${todoId}`);
};

export const editTodo = (todo) => {
    return client.put(`/todos/${todo.id}`, {text: todo.text});
};
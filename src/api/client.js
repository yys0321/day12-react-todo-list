import axios from "axios";

const client = axios.create({
    baseURL: "https://6566e0e664fcff8d730f3364.mockapi.io/api/",
});

export default client;
import { createBrowserRouter } from 'react-router-dom';
import TodoList from '../components/TodoList';
import AboutPage from '../pages/AboutPage';
import DoneList from '../components/DoneList';
import Layout from '../layouts/Layout';
import TodoDetail from '../components/TodoDetail';
import NotFoundPage from '../pages/NotFoundPage';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <NotFoundPage />,
        children: [{
            index: true,
            element: <TodoList />,
        },
        {
            path: "/done",
            element: <DoneList />
        },
        {
            path: "/about",
            element: <AboutPage />
        },
        {
            path: "/todos/:id",
            element: <TodoDetail />
        }]
    }
]);

export default router;
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todos: []
};

const todoListSlice = createSlice({
  name: "todoList",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
    },
    doneTodo: (state, action) => {
      const foundTodo = state.todos.find(todo => todo.id === action.payload);
      foundTodo.done = !foundTodo.done;
    },
    deleteTodo: (state, action) => {
      state.todos = state.todos.filter(todo => todo.id !== action.payload);
    },
    loadItem: (state, action) => {
      state.todos = action.payload;
    },
    editTodo: (state, action) => {
      return state.map((todo) =>
        todo.id === action.payload ? { ...todo, text: todo.text } : todo);
    },
  },
});

export const { addTodo, doneTodo, deleteTodo, loadItem, editTodo } = todoListSlice.actions;
export default todoListSlice.reducer;
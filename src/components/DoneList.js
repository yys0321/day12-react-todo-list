import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import './DoneList.css'

const DoneList = () => {
  const doneItems = useSelector((state) =>
    state.todoList.todos.filter((todoItem) => todoItem.done)
  );

  return (
    <div>
      <h1>Done List</h1>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {doneItems.map((doneItem) => (
          <Link to={`/todos/${doneItem.id}`} key={doneItem.id}>
            <input className='doneListBox' type="text" value={doneItem.text} readOnly />
          </Link>
        ))}
      </div>
    </div>
  );
};

export default DoneList;
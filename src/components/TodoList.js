import React, { useEffect } from 'react';
import { useTodos } from '../hooks/useTodos';
import TodoGenerator from './TodoGenerator';
import TodoGroup from './TodoGroup';

const TodoList = () => {
  const { loadTodos } = useTodos();

  useEffect(() => {
    loadTodos();
  }, []);

  return (
    <div>
      <h1>TodoList</h1>
      <TodoGroup />
      <TodoGenerator />
    </div>
  );
};

export default TodoList;
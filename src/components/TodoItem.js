import React, { useState } from 'react';
import { useTodos } from '../hooks/useTodos';
import { Modal, Input, message } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import './TodoItem.css';

const TodoItem = ({ todo }) => {
    const { toggleTodo, deleteTodo, changeTodo } = useTodos();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [todoText, setTodoText] = useState(todo.text);
    const [isButtonDisabled, setIsButtonDisabled] = useState(true);

    const doneTodoItem = () => {
        const updatedTodoItem = { ...todo, done: !todo.done };
        toggleTodo(updatedTodoItem);
    };

    const deleteTodoItem = () => {
        const confirmDelete = window.confirm('Are you sure you want to delete this todo?');
        if (confirmDelete) {
            deleteTodo(todo);
        }
    };

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        if (todoText.trim() === '') {
            message.warning('Empty input detected. Please enter a valid text.');
            return;
        }

        const updatedTodo = { ...todo, text: todoText };
        changeTodo(updatedTodo);
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const updateTextValue = (event) => {
        const text = event.target.value;
        setTodoText(text);
        setIsButtonDisabled(text.trim() === '');
    };

    return (
        <div>
            <input
                type="text"
                className={todo.done ? 'doneTodoItem' : 'nonDoneTodoItem'}
                onClick={doneTodoItem}
                value={todo.text}
                readOnly
            />
            <span className="deleteTodoItem" onClick={deleteTodoItem}>
                x
            </span>
            <span className="pen" onClick={showModal}>
                <EditOutlined />
            </span>

            <Modal title="Todo Item" visible={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Input
                    placeholder="Please enter the updated text"
                    value={todoText}
                    type="text"
                    onChange={updateTextValue}
                />
            </Modal>
        </div>
    );
};

export default TodoItem;
import React, { useState } from 'react';
import { useTodos } from '../hooks/useTodos';
import './TodoGenerator.css';

const TodoGenerator = () => {
    const [todo, setTodo] = useState('');
    const { createTodo } = useTodos();

    const addList = () => {
        if (todo.trim() !== '') {
            const newTodo = {
                text: todo,
                done: false
            };
            createTodo(newTodo);
            setTodo('');
        };
    };

    const handleInputChange = (event) => {
        setTodo(event.target.value);
    };

    return (
        <div className="insert">
            <input className='inputBox' type="text" value={todo} onChange={handleInputChange} placeholder="input a new todo here..." />
            <button className="addButton" onClick={addList} disabled={todo.trim() === ''}>Add</button>
        </div>
    );
};

export default TodoGenerator;
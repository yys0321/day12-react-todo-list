import React from 'react';
import TodoItem from './TodoItem';
import { useSelector } from 'react-redux';

const TodoGroup = () => {
    const todos = useSelector(state => state.todoList.todos);

    return todos.map((todo) => {
        return <TodoItem key={todo.id} todo={todo} />;
    });
};

export default TodoGroup;
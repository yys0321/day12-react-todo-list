import { Menu } from 'antd';
import { HomeOutlined, CheckCircleOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

const Navigation = () => {
  return (
    <Menu theme="dark" mode="vertical" className="navigation-menu">
      <Menu.Item key="home" icon={<HomeOutlined />} className="nav-item">
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="done" icon={<CheckCircleOutlined />} className="nav-item">
        <Link to="/Done">Done Page</Link>
      </Menu.Item>
      <Menu.Item key="about" icon={<InfoCircleOutlined />} className="nav-item">
        <Link to="/About">About</Link>
      </Menu.Item>
    </Menu>
  );
};

export default Navigation;